import csv
import pprint as pp
import networkx as nx
import itertools as it
import math
import scipy.sparse
import random
import operator




def pagerank(M, N, nodelist, alpha=0.85, personalization=None, max_iter=100, tol=1.0e-6, dangling=None):
	if N == 0:
		return {}
	S = scipy.array(M.sum(axis=1)).flatten()
	S[S != 0] = 1.0 / S[S != 0]
	Q = scipy.sparse.spdiags(S.T, 0, *M.shape, format='csr')
	M = Q * M
	
	# initial vector
	x = scipy.repeat(1.0 / N, N)
	
	# Personalization vector
	if personalization is None:
		p = scipy.repeat(1.0 / N, N)
	else:
		missing = set(nodelist) - set(personalization)
		if missing:
			#raise NetworkXError('Personalization vector dictionary must have a value for every node. Missing nodes %s' % missing)
			print
			print 'Error: personalization vector dictionary must have a value for every node'
			print
			exit(-1)
		p = scipy.array([personalization[n] for n in nodelist], dtype=float)
		#p = p / p.sum()
		sum_of_all_components = p.sum()
		if sum_of_all_components > 1.001 or sum_of_all_components < 0.999:
			print
			print "Error: the personalization vector does not represent a probability distribution :("
			print
			exit(-1)
	
	# Dangling nodes
	if dangling is None:
		dangling_weights = p
	else:
		missing = set(nodelist) - set(dangling)
		if missing:
			#raise NetworkXError('Dangling node dictionary must have a value for every node. Missing nodes %s' % missing)
			print
			print 'Error: dangling node dictionary must have a value for every node.'
			print
			exit(-1)
		# Convert the dangling dictionary into an array in nodelist order
		dangling_weights = scipy.array([dangling[n] for n in nodelist], dtype=float)
		dangling_weights /= dangling_weights.sum()
	is_dangling = scipy.where(S == 0)[0]

	# power iteration: make up to max_iter iterations
	for _ in range(max_iter):
		xlast = x
		x = alpha * (x * M + sum(x[is_dangling]) * dangling_weights) + (1 - alpha) * p
		# check convergence, l1 norm
		err = scipy.absolute(x - xlast).sum()
		if err < N * tol:
			return dict(zip(nodelist, map(float, x)))
	#raise NetworkXError('power iteration failed to converge in %d iterations.' % max_iter)
	print
	print 'Error: power iteration failed to converge in '+str(max_iter)+' iterations.'
	print
	exit(-1)




def create_graph_set_of_users_set_of_items(user_item_ranking_file):
	graph_users_items = {}
	all_users_id = set()
	all_items_id = set()
	g = nx.DiGraph()
	input_file = open(user_item_ranking_file, 'r')
	input_file_csv_reader = csv.reader(input_file, delimiter='\t', quotechar='"', quoting=csv.QUOTE_NONE)
	for line in input_file_csv_reader:
		user_id = int(line[0])
		item_id = int(line[1])
		rating = int(line[2])
		g.add_edge(user_id, item_id, weight=rating)
		all_users_id.add(user_id)
		all_items_id.add(item_id)
	input_file.close()
	graph_users_items['graph'] = g
	graph_users_items['users'] = all_users_id
	graph_users_items['items'] = all_items_id
	return graph_users_items
	












def create_item_item_graph(graph_users_items):
	g = nx.Graph()
	for node in graph_users_items['items']:
		g.add_node(node)

	# Your code here ;)
	for userNode in graph_users_items['users']:
		# print graph_users_items['graph'].neighbors(userNode)
		neighbors = graph_users_items['graph'].neighbors(userNode)
		for node1 in neighbors:
			for node2 in neighbors:
				if node1 != node2:
					g.add_edge(node1,node2)



	return g




def create_preference_vector_for_teleporting(user_id, graph_users_items):
    preference_vector = {}
    ratings = graph_users_items['graph'][user_id]

    for item in ratings:
        preference_vector[item] = float(ratings[item]['weight']) #/(5.0+1.0)

    sumValues = sum(preference_vector.values())

    for item in preference_vector:
        # if preference_vector[item] == 0:
        #     preference_vector[item] = missed
        #     break
        preference_vector[item] = preference_vector[item]/sumValues
        # print preference_vector[item]
    allItems = graph_users_items["items"]
    for item in allItems:
        if item not in preference_vector:
            preference_vector[item] = 0.0
    return preference_vector




def create_ranked_list_of_recommended_items(page_rank_vector_of_items, user_id, training_graph_users_items):
    # This is a list of 'item_id' sorted in descending order of score.
    sorted_list_of_recommended_items = []
    # You can obtain this list from a list of [item, score] couples sorted in descending order of score.
    #  Your code here ;)
    # print "ZZZZ"
    # print  page_rank_vector_of_items
    sorted_ranked_vector = sorted(page_rank_vector_of_items.items(), key=operator.itemgetter(1),reverse=True)

    # print "SORTED RANKED VECTOR"
    # print sorted_ranked_vector
    for item in sorted_ranked_vector:
        neighbors = training_graph_users_items['graph'].neighbors(user_id)
        if item[0] not in neighbors:
            sorted_list_of_recommended_items.append(item[0])


    # print  sorted_list_of_recommended_items

    return sorted_list_of_recommended_items


def create_ranked_list_of_recommended_items_group(page_rank_vector_of_items, user_group, training_graph_users_items):
	sorted_list_of_recommended_items = []
	sorted_ranked_vector = sorted(page_rank_vector_of_items.items(), key=operator.itemgetter(1),reverse=True)

	for item in sorted_ranked_vector:
		isfound = False
		for user_id in user_group:
			neighbors = training_graph_users_items['graph'].neighbors(user_id)
			# print "neighbors"
			# print  neighbors
			if item[0] in neighbors:
				isfound = True
		if not isfound:
			sorted_list_of_recommended_items.append(item[0])
	return sorted_list_of_recommended_items


def discounted_cumulative_gain(user_id, sorted_list_of_recommended_items, test_graph_users_items):
    dcg = 0.
	# Your code here ;)
    i = 1

    ratings = test_graph_users_items['graph'][user_id]
    for item in sorted_list_of_recommended_items:
        if item in ratings:
            dcg += ratings[item]['weight']/math.log(i+1,2)
        i += 1


    return dcg
	



def maximum_discounted_cumulative_gain(user_id, test_graph_users_items):
    dcg = 0.
    # Your code here ;)
    ratings = test_graph_users_items['graph'][user_id]
    neighbors = {}
    for item in ratings:
        neighbors[item] = ratings[item]['weight']


    sorted_ranked_vector = sorted(neighbors.items(), key=operator.itemgetter(1), reverse=True)
    i = 1
    for pair in sorted_ranked_vector:
        dcg += pair[1]/math.log(i+1,2)
        i+=1
    #
    # sorted_neighbors = []
    # for pair in sorted_ranked_vector:
    #     sorted_neighbors.append(pair[0])
    # # print "N"
    # #
    # # print neighbors
    # dcg = discounted_cumulative_gain(user_id, sorted_neighbors, test_graph_users_items)
    # # print "max dcg"
    # # print dcg
    # # print "_____________________________"
    return dcg













